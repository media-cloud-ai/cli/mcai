use std::fmt::{self, Display, Formatter};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
  BackendNameAlreadyTaken(String),
  Confy(confy::ConfyError),
  McaiBenchmark(mcai_benchmark::error::Error),
  Io(std::io::Error),
  McaiClient(mcai_client::Error),
  MissingParameter(String),
  NoBackendWithName(String),
  ParseInt(std::num::ParseIntError),
  Reqwest(reqwest::Error),
  SerdeJson(serde_json::Error),
  McaiDocker(mcai_docker::error::Error),
  GraphRender(mcai_graph_renderer::error::Error),
}

impl std::error::Error for Error {}

impl From<confy::ConfyError> for Error {
  fn from(error: confy::ConfyError) -> Self {
    Error::Confy(error)
  }
}

impl From<mcai_benchmark::error::Error> for Error {
  fn from(error: mcai_benchmark::error::Error) -> Self {
    Error::McaiBenchmark(error)
  }
}

impl From<mcai_client::Error> for Error {
  fn from(error: mcai_client::Error) -> Self {
    Error::McaiClient(error)
  }
}

impl From<reqwest::Error> for Error {
  fn from(error: reqwest::Error) -> Self {
    Error::Reqwest(error)
  }
}

impl From<serde_json::Error> for Error {
  fn from(error: serde_json::Error) -> Self {
    Error::SerdeJson(error)
  }
}

impl From<std::num::ParseIntError> for Error {
  fn from(error: std::num::ParseIntError) -> Self {
    Error::ParseInt(error)
  }
}

impl From<mcai_docker::error::Error> for Error {
  fn from(error: mcai_docker::error::Error) -> Self {
    Error::McaiDocker(error)
  }
}

impl From<std::io::Error> for Error {
  fn from(error: std::io::Error) -> Self {
    Error::Io(error)
  }
}

impl From<mcai_graph_renderer::error::Error> for Error {
  fn from(error: mcai_graph_renderer::error::Error) -> Self {
    Error::GraphRender(error)
  }
}

impl Display for Error {
  fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
    match self {
      Error::BackendNameAlreadyTaken(identifier) => {
        write!(f, "Backend name is already taken: {}", identifier)
      }
      Error::McaiBenchmark(error) => write!(f, "Benchmarking error: {:?}", error),
      Error::McaiClient(error) => write!(f, "MCAI Client error: {}", error),
      Error::Confy(error) => write!(f, "Configuration error: {}", error),
      Error::Io(error) => write!(f, "IO error: {}", error),
      Error::MissingParameter(parameter) => write!(f, "Missing parameter: {}", parameter),
      Error::NoBackendWithName(name) => write!(f, "No backend with name: {}", name),
      Error::ParseInt(error) => write!(f, "Unable to parse number: {}", error),
      Error::Reqwest(error) => write!(f, "HTTP request: {}", error),
      Error::SerdeJson(error) => write!(f, "JSON: {}", error),
      Error::McaiDocker(error) => write!(f, "Docker {:?}", error),
      Error::GraphRender(error) => write!(f, "Graph rendering {}", error),
    }
  }
}
