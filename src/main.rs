mod configuration;
mod constant;
mod datetime;
mod error;
mod output;
mod pagination;
mod renderer;
mod subcommands;

use crate::subcommands::{get_confirmation, GlobalFlags, SubCommands};
use clap::Parser;
use constant::*;
use env_logger::Builder;
use error::{Error, Result};
use output::Output;
use std::io::Write;

#[tokio::main]
async fn main() -> Result<()> {
  /// Interact with Media Cloud AI platform
  #[derive(Parser)]
  #[clap(name = "mcai", version, author)]
  struct Mcai {
    /// Disable confirmation acknowledgement
    #[clap(short = 'y', long, global = true)]
    skip_acknowledgement: bool,

    #[clap(subcommand)]
    command: SubCommands,
  }
  let mut builder = Builder::from_default_env();
  builder
    .format(move |stream, record| writeln!(stream, "[{}] {}", record.level(), record.args(),))
    .init();

  let configuration = configuration::Configuration::load()?;
  let matches = Mcai::parse();
  let global_flags = GlobalFlags {
    skip_acknowledgement: matches.skip_acknowledgement,
  };

  match matches.command {
    SubCommands::Configuration { command } => command.execute(configuration, global_flags).await?,
    SubCommands::Credential { command } => command.execute(configuration, global_flags).await?,
    SubCommands::User { command } => command.execute(configuration, global_flags).await?,
    SubCommands::Worker { command } => command.execute(configuration, global_flags).await?,
    SubCommands::Workflow { command } => command.execute(configuration, global_flags).await?,
  };
  Ok(())
}

async fn print_response<T>(response: reqwest::Response, output: &Output) -> Result<()>
where
  T: for<'de> serde::Deserialize<'de> + renderer::Render,
{
  match output {
    Output::Formatted => {
      let parsed_items: T = response.json().await?;
      parsed_items.render();
    }
    Output::Json(false) => {
      let content: serde_json::Value = response.json().await?;
      println!("{}", serde_json::to_string(&content)?);
    }
    Output::Json(true) => {
      let content: serde_json::Value = response.json().await?;
      println!("{}", serde_json::to_string_pretty(&content)?);
    }
    Output::Raw => {
      println!("{}", response.text().await?);
    }
  }
  Ok(())
}
