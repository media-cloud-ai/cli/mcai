mod backend;
use crate::{built_info::PKG_NAME, Error, Result};
pub use backend::Backend;
use directories::ProjectDirs;
use mcai_client::Client;
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Deserialize, Serialize)]
pub struct Configuration {
  backends: Vec<Backend>,
}

impl Configuration {
  pub fn load() -> Result<Self> {
    Ok(confy::load(PKG_NAME)?)
  }

  pub fn save(&self) -> Result<()> {
    confy::store(PKG_NAME, self)?;

    let project = ProjectDirs::from("rs", "", PKG_NAME).unwrap();
    log::info!(
      "Configuration stored in folder: {}",
      project.preference_dir().display()
    );
    Ok(())
  }

  pub fn add_backend(&mut self, backend: Backend) -> Result<()> {
    if self.get_backend(&backend.name).is_ok() {
      return Err(Error::BackendNameAlreadyTaken(backend.name));
    }

    self.backends.push(backend);
    Ok(())
  }

  pub fn delete_backend(&mut self, identifier: &str) -> Result<()> {
    if let Some(index) = self
      .backends
      .iter()
      .enumerate()
      .find_map(|(index, backend)| {
        if backend.name == identifier {
          Some(index)
        } else {
          None
        }
      })
    {
      self.backends.remove(index);

      Ok(())
    } else {
      Err(Error::NoBackendWithName(identifier.to_string()))
    }
  }

  pub fn list_backends(&self) {
    for backend in &self.backends {
      println!("## Backend ##");
      println!("{}", backend);
    }
  }

  pub fn show_backend(&self, identifier: &str) -> Result<()> {
    let backend = self.get_backend(identifier)?;
    println!("## Backend ##");
    println!("{}", backend);
    Ok(())
  }

  pub fn get_backend(&self, identifier: &str) -> Result<&Backend> {
    if let Some(backend) = self
      .backends
      .iter()
      .find(|backend| backend.name == identifier)
    {
      Ok(backend)
    } else {
      Err(Error::NoBackendWithName(identifier.to_string()))
    }
  }

  pub fn get_backend_client(&self, identifier: &str) -> Result<Client> {
    let backend_configuration = self.get_backend(identifier)?;
    Ok(Client::from(backend_configuration.clone()))
  }
}
