use crate::HIDDEN_VALUE;
use mcai_client::{Client, Login};
use serde::{Deserialize, Serialize};
use std::fmt;

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct Backend {
  pub name: String,
  pub hostname: String,
  pub(crate) password: String,
  pub(crate) username: String,
}

impl Backend {
  pub fn get_session_url(&self) -> String {
    self.build_url("/api/sessions")
  }

  pub fn build_url(&self, endpoint: &str) -> String {
    format!("{}{}", self.hostname, endpoint)
  }

  pub fn new(name: &str, hostname: &str, password: &str, username: &str) -> Self {
    Backend {
      name: name.to_string(),
      hostname: hostname.to_string(),
      password: password.to_string(),
      username: username.to_string(),
    }
  }
}

impl fmt::Display for Backend {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    writeln!(f, "name    : {}", self.name)?;
    writeln!(f, "hostname: {}", self.hostname)?;
    writeln!(f, "username: {}", self.username)?;
    writeln!(f, "password: {}", HIDDEN_VALUE)
  }
}

impl From<&Backend> for Login {
  fn from(configuration: &Backend) -> Self {
    Login::new(&configuration.username, &configuration.password)
  }
}

impl From<Backend> for Client {
  fn from(backend: Backend) -> Self {
    Client::new(
      backend.hostname.clone(),
      backend.username.clone(),
      backend.password,
    )
  }
}
