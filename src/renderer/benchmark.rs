use crate::renderer::Render;
use serde::Deserialize;
use term_table::row::Row;
use term_table::table_cell::{Alignment, TableCell};
use term_table::{Table, TableStyle};

#[derive(Deserialize)]
pub struct BenchmarksListRenderer {
  #[serde(flatten)]
  pub benchmarks: Vec<String>,
}

impl Render for BenchmarksListRenderer {
  fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::simple();
    table.add_row(benchmarks_list_columns());

    for benchmark in &self.benchmarks {
      table.add_row(benchmarks_list_rows(benchmark.clone()));
    }

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
      format!("{} benchmarks available", self.benchmarks.len()),
      1,
      Alignment::Right,
    )]));

    println!("{}", table.render());
  }
}

pub fn benchmarks_list_columns<'a>() -> Row<'a> {
  Row::new(vec![TableCell::new_with_alignment(
    "Benchmark names",
    1,
    Alignment::Center,
  )])
}

pub fn benchmarks_list_rows<'a>(benchmark: String) -> Row<'a> {
  Row::new(vec![TableCell::new(benchmark)])
}
