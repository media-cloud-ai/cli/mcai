mod benchmark;
mod credential;
mod user;
mod worker;
mod workflow;

pub use benchmark::BenchmarksListRenderer;
pub use credential::{
  credential_columns, credential_rows, credential_rows_hidden_value, CredentialListRenderer,
  CredentialShowRenderer,
};
pub use user::{user_columns, user_rows, UserListRenderer, UserShowRenderer};
pub use worker::{worker_columns, worker_rows, WorkerListRenderer, WorkerShowRenderer};
pub use workflow::{workflow_columns, workflow_rows, WorkflowListRenderer, WorkflowShowRenderer};

pub trait Render {
  fn render(&self);
}
