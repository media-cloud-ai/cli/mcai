use crate::renderer::Render;
use mcai_client::{UserList, UserShow};
use mcai_models::User;
use serde_derive::Deserialize;
use term_table::{
  row::Row,
  table_cell::{Alignment, TableCell},
  Table, TableStyle,
};

#[derive(Deserialize)]
pub struct UserShowRenderer {
  #[serde(flatten)]
  user_single: UserShow,
}

impl Render for UserShowRenderer {
  fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(user_columns());
    table.add_row(user_rows(&self.user_single.data));
    println!("{}", table.render());
  }
}

#[derive(Deserialize)]
pub struct UserListRenderer {
  #[serde(flatten)]
  user_multiple: UserList,
}

impl Render for UserListRenderer {
  fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(user_columns());

    for user in &self.user_multiple.data {
      table.add_row(user_rows(user));
    }

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
      format!(
        "{}-{}/{}",
        0,
        self.user_multiple.data.len(),
        self.user_multiple.total
      ),
      2,
      Alignment::Right,
    )]));

    println!("{}", table.render());
  }
}

pub fn user_columns<'a>() -> Row<'a> {
  Row::new(vec![
    TableCell::new("ID"),
    TableCell::new("Email"),
    TableCell::new("Rights"),
  ])
}

pub fn user_rows<'a>(user: &User) -> Row<'a> {
  Row::new(vec![
    TableCell::new(&user.id),
    TableCell::new(&user.email),
    TableCell::new(&user.roles.join(", ")),
  ])
}
