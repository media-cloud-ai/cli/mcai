use crate::renderer::Render;
use mcai_client::{WorkerList, WorkerShow};
use mcai_models::Worker;
use serde_derive::Deserialize;
use term_table::{
  row::Row,
  table_cell::{Alignment, TableCell},
  Table, TableStyle,
};

#[derive(Deserialize)]
pub struct WorkerShowRenderer {
  #[serde(flatten)]
  worker_single: WorkerShow,
}

impl Render for WorkerShowRenderer {
  fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(worker_columns());
    table.add_row(worker_rows(&self.worker_single.data));
    println!("{}", table.render());
  }
}

#[derive(Deserialize)]
pub struct WorkerListRenderer {
  #[serde(flatten)]
  worker_multiple: WorkerList,
}

impl Render for WorkerListRenderer {
  fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(worker_columns());

    for worker in &self.worker_multiple.data {
      table.add_row(worker_rows(worker));
    }

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
      format!(
        "{}-{}/{}",
        0,
        self.worker_multiple.data.len(),
        self.worker_multiple.total
      ),
      4,
      Alignment::Right,
    )]));

    println!("{}", table.render());
  }
}

pub fn worker_columns<'a>() -> Row<'a> {
  Row::new(vec![
    TableCell::new("ID"),
    TableCell::new("Activity"),
    TableCell::new("Version"),
    TableCell::new("Queue Name"),
  ])
}

pub fn worker_rows<'a>(worker: &Worker) -> Row<'a> {
  Row::new(vec![
    TableCell::new(&worker.instance_id),
    TableCell::new(&worker.activity),
    TableCell::new(&worker.version),
    TableCell::new(&worker.queue_name),
  ])
}
