use crate::{constant::HIDDEN_VALUE, renderer::Render};
use mcai_client::{CredentialList, CredentialShow};
use mcai_models::Credential;
use serde_derive::Deserialize;
use term_table::{
  row::Row,
  table_cell::{Alignment, TableCell},
  Table, TableStyle,
};

#[derive(Deserialize)]
pub struct CredentialShowRenderer {
  #[serde(flatten)]
  credential_single: CredentialShow,
}

impl Render for CredentialShowRenderer {
  fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(credential_columns());
    table.add_row(credential_rows(&self.credential_single.data));
    println!("{}", table.render());
  }
}

#[derive(Deserialize)]
pub struct CredentialListRenderer {
  #[serde(flatten)]
  credential_multiple: CredentialList,
}

impl Render for CredentialListRenderer {
  fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(credential_columns());

    for credential in &self.credential_multiple.data {
      table.add_row(credential_rows_hidden_value(credential));
    }

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
      format!(
        "{}-{}/{}",
        0,
        self.credential_multiple.data.len(),
        self.credential_multiple.total
      ),
      4,
      Alignment::Right,
    )]));

    println!("{}", table.render());
  }
}

pub fn credential_rows_hidden_value<'a>(credential: &Credential) -> Row<'a> {
  Row::new(vec![
    TableCell::new(format!("{}", credential.id)),
    TableCell::new(&credential.inserted_at),
    TableCell::new(credential.key.to_string()),
    TableCell::new(HIDDEN_VALUE.to_string()),
  ])
}

pub fn credential_rows(credential: &Credential) -> Row {
  Row::new(vec![
    TableCell::new(format!("{}", credential.id)),
    TableCell::new(&credential.inserted_at),
    TableCell::new(credential.key.to_string()),
    TableCell::new(credential.value.to_string()),
  ])
}

pub fn credential_columns<'a>() -> Row<'a> {
  Row::new(vec![
    TableCell::new("ID"),
    TableCell::new("Created at"),
    TableCell::new("Key"),
    TableCell::new("Value"),
  ])
}

#[cfg(test)]
mod test {
  use super::*;
  use term_table::{row::Row, table_cell::TableCell};

  #[test]
  fn from_credential_to_row() {
    let credential = Credential {
      inserted_at: "test_date".to_string(),
      id: 1,
      key: "test_key".to_string(),
      value: "test_value".to_string(),
    };

    let row = Row {
      cells: vec![
        TableCell::new("1"),
        TableCell::new("test_date"),
        TableCell::new("test_key"),
        TableCell::new("test_value"),
      ],
      has_separator: true,
    };

    let result = credential_rows(&credential);
    let row_string = format!("{:?}", row);
    let result_string = format!("{:?}", result);
    assert_eq!(result_string, row_string);
  }
}
