use crate::renderer::Render;
use mcai_client::{WorkflowList, WorkflowShow};
use mcai_models::WorkflowInstance;
use serde_derive::Deserialize;
use term_table::{
  row::Row,
  table_cell::{Alignment, TableCell},
  Table, TableStyle,
};

#[derive(Deserialize)]
pub struct WorkflowShowRenderer {
  #[serde(flatten)]
  workflow_single: WorkflowShow,
}

impl Render for WorkflowShowRenderer {
  fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(workflow_columns());
    table.add_row(workflow_rows(&self.workflow_single.data));
    println!("{}", table.render());
  }
}

#[derive(Deserialize)]
pub struct WorkflowListRenderer {
  #[serde(flatten)]
  workflow_multiple: WorkflowList,
}
impl Render for WorkflowListRenderer {
  fn render(&self) {
    let mut table = Table::new();
    table.style = TableStyle::rounded();
    table.add_row(workflow_columns());

    for workflow in &self.workflow_multiple.data {
      table.add_row(workflow_rows(workflow));
    }

    table.add_row(Row::new(vec![TableCell::new_with_alignment(
      format!(
        "{}-{}/{}",
        0,
        self.workflow_multiple.data.len(),
        self.workflow_multiple.total
      ),
      6,
      Alignment::Right,
    )]));

    println!("{}", table.render());
  }
}

pub fn workflow_columns<'a>() -> Row<'a> {
  Row::new(vec![
    TableCell::new("ID"),
    TableCell::new("Created at"),
    TableCell::new("Identifier"),
    TableCell::new("Version"),
    TableCell::new("Schema Version"),
    TableCell::new("State"),
    TableCell::new("Reference"),
  ])
}

pub fn workflow_rows<'a>(workflow: &WorkflowInstance) -> Row<'a> {
  Row::new(vec![
    TableCell::new(format!("{}", workflow.id)),
    TableCell::new(&workflow.created_at),
    TableCell::new(&workflow.identifier),
    TableCell::new(format!(
      "{}.{}.{}",
      workflow.version_major, workflow.version_minor, workflow.version_micro
    )),
    TableCell::new(&workflow.schema_version),
    TableCell::new(
      workflow
        .status
        .as_ref()
        .map(|status| status.state.clone())
        .unwrap_or_else(|| "-".to_string()),
    ),
    TableCell::new(workflow.reference.clone().unwrap_or_default()),
  ])
}

#[cfg(test)]
mod test {
  use crate::renderer::workflow::workflow_rows;
  use chrono::NaiveDate;
  use mcai_models::{Status, WorkflowInstance};
  use term_table::{row::Row, table_cell::TableCell};

  #[test]
  fn from_workflow_to_row() {
    let workflow = WorkflowInstance {
      label: "Test workflow".to_string(),
      created_at: NaiveDate::from_ymd(2015, 9, 5).and_hms(23, 56, 4),
      id: 1,
      identifier: "test_id".to_string(),
      is_live: false,
      jobs: vec![],
      reference: None,
      schema_version: "test_schema_version".to_string(),
      status: Option::from(Status {
        id: 0,
        inserted_at: NaiveDate::from_ymd(2015, 9, 5).and_hms(23, 56, 4),
        state: "pending".to_string(),
      }),
      version_major: 1,
      version_minor: 1,
      version_micro: 1,
      start_parameters: vec![],
      steps: vec![],
      tags: vec![],
    };

    let row = Row {
      cells: vec![
        TableCell::new("1"),
        TableCell::new("2015-09-05 23:56:04"),
        TableCell::new("test_id"),
        TableCell::new("1.1.1"),
        TableCell::new("test_schema_version"),
        TableCell::new("pending"),
        TableCell::new(""),
      ],
      has_separator: true,
    };

    let result = workflow_rows(&workflow);
    let row_string = format!("{:?}", row);
    let result_string = format!("{:?}", result);

    assert_eq!(result_string, row_string);
  }
}
