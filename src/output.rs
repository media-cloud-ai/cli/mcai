type Pretty = bool;

#[derive(Debug)]
pub enum Output {
  Formatted,
  Json(Pretty),
  Raw,
}

impl From<&str> for Output {
  fn from(value: &str) -> Self {
    match value {
      "json" => Output::Json(false),
      "pretty-json" => Output::Json(true),
      "formatted" => Output::Formatted,
      _ => Output::Raw,
    }
  }
}
