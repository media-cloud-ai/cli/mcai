use crate::{Error, Result};
use chrono::prelude::*;
use std::str::FromStr;

pub fn parse_date(input: &str) -> Result<DateTime<Utc>> {
  if let Ok(date_time_utc) = DateTime::<Utc>::from_str(input) {
    return Ok(date_time_utc);
  }
  if let Ok(date_time) = NaiveDateTime::parse_from_str(input, "%Y-%m-%d %H:%M:%S") {
    return Ok(DateTime::<Utc>::from_utc(date_time, Utc));
  }
  if let Ok(date) = NaiveDate::parse_from_str(input, "%Y-%m-%d") {
    return Ok(DateTime::<Utc>::from_utc(
      NaiveDateTime::new(date, NaiveTime::from_hms(0, 0, 0)),
      Utc,
    ));
  }
  if let Ok(time) = NaiveTime::parse_from_str(input, "%H:%M:%S") {
    return Ok(DateTime::<Utc>::from_utc(
      NaiveDateTime::new(Utc::today().naive_utc(), time),
      Utc,
    ));
  }
  Err(Error::MissingParameter(
    "Incorrect date entry. Try date time, date or time".to_string(),
  ))
}
