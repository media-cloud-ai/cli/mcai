use crate::{
  configuration::Backend,
  configuration::Configuration,
  error::Result,
  subcommands::{get_confirmation_from_vec, user_input_with_field_name},
  GlobalFlags, Output, OPERATION_DELETE,
};
use clap::Subcommand;
use rpassword::prompt_password_stdout;

#[derive(Debug, Subcommand)]
pub enum BackendSubcommands {
  /// Add a new backend
  Add {
    /// Sets the new backend name
    #[clap(required = true)]
    name: String,
    /// Sets the new backend hostname
    #[clap(long, short = 'H')]
    hostname: Option<String>,
    /// Sets the new backend password
    #[clap(long, short)]
    password: Option<String>,
    /// Sets the new backend username
    #[clap(long, short)]
    username: Option<String>,
  },
  /// Delete one or more specified backend
  Delete {
    /// Sets identifiers    
    #[clap(multiple_values = true, index = 1, required = true)]
    identifiers: Vec<String>,
  },
  /// List backends
  List {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Configure the rendering output
    #[clap(short, default_value = "formatted", parse(from_str))]
    output: Output,
  },
  /// Show one or more specified backends
  Show {
    /// Sets the backend
    #[clap(long, short, default_value = "default")]
    backend: String,
    /// Sets identifiers
    #[clap(multiple_values = true, index = 1, required = true)]
    identifiers: Vec<String>,
    /// Configure the rendering output
    #[clap(short, long, default_value = "formatted", parse(from_str))]
    output: Output,
  },
}

impl BackendSubcommands {
  pub fn execute(&self, configuration: Configuration, global_flags: GlobalFlags) -> Result<()> {
    match self {
      BackendSubcommands::Add {
        name,
        hostname,
        password,
        username,
      } => Self::handle_add(configuration, name, hostname, password, username),
      BackendSubcommands::Delete { identifiers } => {
        Self::handle_delete(configuration, identifiers, global_flags)
      }
      BackendSubcommands::List { .. } => Self::handle_list(configuration),
      BackendSubcommands::Show { identifiers, .. } => Self::handle_show(configuration, identifiers),
    }
  }

  fn handle_add(
    mut configuration: Configuration,
    name: &str,
    hostname: &Option<String>,
    password: &Option<String>,
    username: &Option<String>,
  ) -> Result<()> {
    let hostname_value = hostname
      .clone()
      .unwrap_or_else(|| user_input_with_field_name("hostname"));

    let password_value = password
      .clone()
      .unwrap_or_else(|| prompt_password_stdout("Enter password: ").unwrap());

    let username_value = username
      .clone()
      .unwrap_or_else(|| user_input_with_field_name("username"));

    let new_backend = Backend::new(name, &hostname_value, &password_value, &username_value);

    if configuration.add_backend(new_backend).is_ok() {
      let _ = configuration.save();
      println!("Backend {} added.", name);
    } else {
      println!("Failed add backend {}", name);
    }
    Ok(())
  }

  fn handle_delete(
    mut configuration: Configuration,
    identifiers: &[String],
    global_flags: GlobalFlags,
  ) -> Result<()> {
    if global_flags.skip_acknowledgement
      || get_confirmation_from_vec(OPERATION_DELETE, "backend", identifiers)
    {
      for id in identifiers {
        if configuration.delete_backend(id).is_ok() {
          let _ = configuration.save();
          println!("Backend {} deleted.", id)
        } else {
          println!("Failed delete backend {}", id)
        }
      }
    }
    Ok(())
  }

  fn handle_list(configuration: Configuration) -> Result<()> {
    configuration.list_backends();
    Ok(())
  }

  fn handle_show(configuration: Configuration, identifiers: &[String]) -> Result<()> {
    for id in identifiers {
      configuration.show_backend(id)?;
    }
    Ok(())
  }
}
