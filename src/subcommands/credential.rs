use crate::renderer::{CredentialListRenderer, CredentialShowRenderer};
use crate::Error::MissingParameter;
use crate::{
  configuration::Configuration,
  constant::*,
  error::Result,
  pagination::PaginationOption,
  print_response,
  subcommands::{get_confirmation, GlobalFlags},
  Output,
};
use clap::Subcommand;
use mcai_client::{Action, Client, Pagination};
use mcai_models::CredentialContent;
use rpassword::prompt_password_stdout;

#[derive(Debug, Subcommand)]
pub enum CredentialSubcommands {
  /// Add a new credential
  Add {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets key
    #[clap(required = true)]
    key: String,
    /// Sets password
    #[clap(short, long)]
    password: Option<String>,
  },
  /// Delete a specified credential
  Delete {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets key
    #[clap(required = true)]
    identifier: i64,
  },
  /// List credentials
  List {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Configure the rendering output
    #[clap(short, default_value = "formatted", parse(from_str))]
    output: Output,
    #[clap(flatten)]
    pagination: PaginationOption,
  },
  /// Show one or more specified credentials
  Show {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets key
    #[clap(required = true)]
    identifiers: Vec<String>,
    /// Configure the rendering output
    #[clap(short, default_value = "formatted", parse(from_str))]
    output: Output,
  },
}

impl CredentialSubcommands {
  pub(crate) async fn execute(
    &self,
    configuration: Configuration,
    global_flags: GlobalFlags,
  ) -> Result<()> {
    match self {
      CredentialSubcommands::Add {
        backend,
        key,
        password,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        Self::handle_add(key, password, backend_client).await
      }
      CredentialSubcommands::Delete {
        backend,
        identifier,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        Self::handle_delete(identifier, backend_client, global_flags).await
      }
      CredentialSubcommands::List {
        output,
        backend,
        pagination,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        Self::handle_list(backend_client, output, pagination).await
      }
      CredentialSubcommands::Show {
        identifiers,
        output,
        backend,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        Self::handle_show(identifiers, backend_client, output).await
      }
    }
  }

  async fn handle_add(key: &str, password: &Option<String>, mut backend: Client) -> Result<()> {
    let value = if let Some(value) = password {
      value.clone()
    } else {
      prompt_password_stdout("Password: ").map_err(|err| MissingParameter(err.to_string()))?
    };

    let response = backend
      .request(Action::AddCredential(CredentialContent {
        key: key.to_string(),
        value,
      }))
      .await?;

    if response.status().is_success() {
      println!("Credential '{}' added!", key);
    } else {
      println!(
        "Credential '{}' not created, got response status: {}",
        key,
        response.status()
      )
    }
    Ok(())
  }

  async fn handle_delete(
    identifier: &i64,
    mut backend_client: Client,
    global_flags: GlobalFlags,
  ) -> Result<()> {
    if global_flags.skip_acknowledgement
      || get_confirmation(OPERATION_DELETE, "Credential", &identifier.to_string())
    {
      let response = backend_client
        .request(Action::DeleteCredential {
          identifier: *identifier,
        })
        .await?;

      if response.status().is_success() {
        println!("Credential '{}' deleted!", identifier);
      } else {
        println!(
          "Credential '{}' not deleted, got response status: {}",
          identifier,
          response.status()
        )
      }
    } else {
      println!("Cancelled operation");
    }
    Ok(())
  }

  async fn handle_list(
    mut backend_client: Client,
    output: &Output,
    pagination: &PaginationOption,
  ) -> Result<()> {
    let response = backend_client
      .request(Action::ListCredential {
        pagination: Pagination::from(pagination.clone()),
      })
      .await?;
    print_response::<CredentialListRenderer>(response, output).await?;
    Ok(())
  }

  async fn handle_show(
    identifiers: &[String],
    mut backend_client: Client,
    output: &Output,
  ) -> Result<()> {
    for identifier in identifiers.iter() {
      let response = backend_client
        .request(Action::ShowCredential {
          identifier: identifier.clone(),
        })
        .await?;
      print_response::<CredentialShowRenderer>(response, output).await?;
    }
    Ok(())
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::configuration::Backend;
  use httpmock::prelude::*;
  use mcai_client::{AccessToken, CredentialShow};
  use mcai_models::Credential;

  #[tokio::test]
  async fn test_execute() {
    let expected_credential = CredentialShow {
      data: Credential {
        inserted_at: "2021-10-28T12:28:57".to_string(),
        id: 123,
        key: "KEY".to_string(),
        value: "VALUE".to_string(),
      },
    };

    let server = MockServer::start();
    server.mock(|when, then| {
      when.method(GET).path("/api/credentials/KEY");
      then
        .status(200)
        .header("content-type", "application/json")
        .body(serde_json::to_string(&expected_credential).unwrap());
    });

    let access_token = AccessToken {
      access_token: "".to_string(),
      user: Default::default(),
    };
    server.mock(|when, then| {
      when.method(POST).path("/api/sessions");
      then
        .status(200)
        .header("content-type", "application/json")
        .body(serde_json::to_string(&access_token).unwrap());
    });

    let mut configuration = Configuration::default();
    configuration
      .add_backend(Backend::new(
        "backend_name",
        &server.base_url(),
        "user",
        "password",
      ))
      .unwrap();

    let output = Output::Raw;

    let credential_subcommand = CredentialSubcommands::Show {
      backend: "backend_name".to_string(),
      identifiers: vec!["KEY".to_string()],
      output,
    };

    let global_flag = GlobalFlags {
      skip_acknowledgement: true,
    };

    let result = credential_subcommand.execute(configuration, global_flag);
    assert!(result.await.is_ok());
  }
}
