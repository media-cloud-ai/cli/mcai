use crate::{
  configuration::Configuration,
  error::Result,
  pagination::PaginationOption,
  print_response,
  renderer::{UserListRenderer, UserShowRenderer},
  GlobalFlags, Output,
};
use clap::Subcommand;
use mcai_client::{Action, Client, Pagination};

#[derive(Debug, Subcommand)]
pub enum UserSubcommands {
  /// List users
  List {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Configure the rendering output
    #[clap(short, default_value = "formatted", parse(from_str))]
    output: Output,
    #[clap(flatten)]
    pagination: PaginationOption,
  },
  /// Show one or more specified users
  Show {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets user identifier
    #[clap(multiple_values = true, index = 1, required = true)]
    identifiers: Vec<i64>,
    /// Configure the rendering output
    #[clap(short, default_value = "formatted", parse(from_str))]
    output: Output,
  },
}

impl UserSubcommands {
  pub async fn execute(
    &self,
    configuration: Configuration,
    _global_flags: GlobalFlags,
  ) -> Result<()> {
    match self {
      UserSubcommands::List {
        output,
        backend,
        pagination,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        Self::handle_list(backend_client, output, pagination).await
      }
      UserSubcommands::Show {
        output,
        backend,
        identifiers,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        Self::handle_show(backend_client, output, identifiers).await
      }
    }
  }

  async fn handle_list(
    mut backend_client: Client,
    output: &Output,
    pagination: &PaginationOption,
  ) -> Result<()> {
    let response = backend_client
      .request(Action::ListUser {
        pagination: Pagination::from(pagination.clone()),
      })
      .await?;
    print_response::<UserListRenderer>(response, output).await?;
    Ok(())
  }

  async fn handle_show(
    mut backend_client: Client,
    output: &Output,
    identifiers: &[i64],
  ) -> Result<()> {
    for id in identifiers {
      let response = backend_client
        .request(Action::ShowUser { identifier: *id })
        .await?;
      print_response::<UserShowRenderer>(response, output).await?;
    }
    Ok(())
  }
}
