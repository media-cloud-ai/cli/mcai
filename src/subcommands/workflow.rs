use crate::{
  configuration::Configuration,
  datetime::parse_date,
  error::Result,
  pagination::PaginationOption,
  print_response,
  renderer::{WorkflowListRenderer, WorkflowShowRenderer},
  subcommands::get_confirmation_from_vec,
  GlobalFlags, Output, OPERATION_ABORT, OPERATION_DELETE, OPERATION_STOP,
};
use chrono::{DateTime, Utc};
use clap::Subcommand;
use mcai_client::{Action, Client, Pagination, ShowWorkflowMode, WorkflowList};
use mcai_graph_renderer::{
  Output as GraphRenderOutput, OutputFormat, SvgOutput, WorkflowRenderer, WorkflowRendererOptions,
};

#[derive(Debug, Subcommand)]
pub enum WorkflowSubcommands {
  /// Abort one or more specified workflows
  Abort {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets identifiers
    #[clap(multiple_values = true, name = "identifiers", required_unless_present_any = &["after_date", "before_date", "name", "states"])]
    identifiers: Vec<i64>,
    /// Sets start date filter
    #[clap(long, short, name = "after_date", parse(try_from_str=parse_date), required_unless_present_any = &["identifiers", "before_date", "name", "states"])]
    after_date: Option<DateTime<Utc>>,
    /// Sets end date filter
    #[clap(long, short, name = "before_date", parse(try_from_str=parse_date), required_unless_present_any = &["identifiers", "after_date", "name", "states"])]
    before_date: Option<DateTime<Utc>>,
    /// Sets name filter
    #[clap(
      long,
      short,
      name = "name",
      multiple_values = true,
      required_unless_present_any = &["identifiers", "after_date", "before_date", "states"]
      )]
    name: Option<String>,
    /// Sets state filter
    #[clap(
      long,
      short,
      name = "states",
      multiple_values = true,
      required_unless_present_any = &["identifiers", "after_date", "before_date", "name"]
      )]
    states: Vec<String>,
  },
  /// Delete one or more specified workflow
  Delete {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets identifiers
    #[clap(multiple_values = true, name = "identifiers", required_unless_present_any = &["after_date", "before_date", "name", "states"])]
    identifiers: Vec<i64>,
    /// Sets start date filter
    #[clap(long, short, name = "after_date", parse(try_from_str=parse_date), required_unless_present_any = &["identifiers", "before_date", "name", "states"])]
    after_date: Option<DateTime<Utc>>,
    /// Sets end date filter
    #[clap(long, short, name = "before_date", parse(try_from_str=parse_date), required_unless_present_any = &["identifiers", "after_date", "name", "states"])]
    before_date: Option<DateTime<Utc>>,
    /// Sets name filter
    #[clap(
      long,
      short,
      name = "name",
      multiple_values = true,
      required_unless_present_any = &["identifiers", "after_date", "before_date", "states"]
    )]
    name: Option<String>,
    /// Sets state filter
    #[clap(
      long,
      short,
      name = "states",
      multiple_values = true,
      required_unless_present_any = &["identifiers", "after_date", "before_date", "name"]
    )]
    states: Vec<String>,
  },
  /// List workflows
  List {
    /// Sets start date filter
    #[clap(long, short, parse(try_from_str=parse_date))]
    after_date: Option<DateTime<Utc>>,
    /// Sets end date filter
    #[clap(long, short, parse(try_from_str=parse_date))]
    before_date: Option<DateTime<Utc>>,
    /// Sets name filter
    #[clap(long, short, multiple_values = true)]
    name: Option<String>,
    /// Sets state filter
    #[clap(long, short, multiple_values = true)]
    states: Vec<String>,
    /// Configure the rendering output
    #[clap(short, default_value = "formatted", parse(from_str))]
    output: Output,
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    #[clap(flatten)]
    pagination: PaginationOption,
  },
  /// Render workflow graph
  Render {
    /// Path to the workflow JSON file
    #[clap(short, long, name("input-file"))]
    input_file: String,
    /// Output format
    #[clap(short, long, value_enum)]
    format: OutputFormat,
    /// Path to the outputfile
    #[clap(short, long, name("output-file"))]
    output_file: String,
    /// Width of the nodes
    #[clap(long, default_value_t = 300)]
    width: usize,
    /// Height of the nodes
    #[clap(long, default_value_t = 50)]
    height: usize,
    /// X gap of the nodes
    #[clap(long, default_value_t = 50, name("x-gap"))]
    x_gap: usize,
    /// Y gap of the nodes
    #[clap(long, default_value_t = 50, name("y-gap"))]
    y_gap: usize,
  },
  /// Show one or more specified workflows
  Show {
    /// Sets identifiers
    #[clap(required = true)]
    identifiers: Vec<i64>,
    /// Configure the rendering output
    #[clap(short, default_value = "formatted", parse(from_str))]
    output: Output,
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
  },
  /// Stop one or more specified workflow
  Stop {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets identifiers
    #[clap(multiple_values = true, name = "identifiers", required_unless_present_any = &["after_date", "before_date", "name", "states"])]
    identifiers: Vec<i64>,
    /// Sets start date filter
    #[clap(long, short, name = "after_date", parse(try_from_str=parse_date), required_unless_present_any = &["identifiers", "before_date", "name", "states"])]
    after_date: Option<DateTime<Utc>>,
    /// Sets end date filter
    #[clap(long, short, name = "before_date", parse(try_from_str=parse_date), required_unless_present_any = &["identifiers", "after_date", "name", "states"])]
    before_date: Option<DateTime<Utc>>,
    /// Sets name filter
    #[clap(
    long,
    short,
    name = "name",
    multiple_values = true,
    required_unless_present_any = &["identifiers", "after_date", "before_date", "states"]
    )]
    name: Option<String>,
    /// Sets state filter
    #[clap(
    long,
    short,
    name = "states",
    multiple_values = true,
    required_unless_present_any = &["identifiers", "after_date", "before_date", "name"]
    )]
    states: Vec<String>,
  },
}

impl WorkflowSubcommands {
  pub async fn execute(
    &self,
    configuration: Configuration,
    global_flags: GlobalFlags,
  ) -> Result<()> {
    match self {
      WorkflowSubcommands::List {
        after_date,
        before_date,
        name,
        states,
        output,
        backend,
        pagination,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        Self::handle_list(
          backend_client,
          output,
          after_date,
          before_date,
          name,
          states,
          pagination,
        )
        .await
      }

      WorkflowSubcommands::Show {
        identifiers,
        output,
        backend,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        Self::handle_show(backend_client, output, identifiers).await
      }
      WorkflowSubcommands::Delete {
        backend,
        identifiers,
        after_date,
        before_date,
        name,
        states,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        let ids = get_identifiers(
          identifiers,
          after_date,
          before_date,
          name,
          states,
          backend,
          configuration,
        )
        .await?;
        Self::handle_delete(backend_client, &global_flags, ids).await
      }

      WorkflowSubcommands::Abort {
        backend,
        identifiers,
        after_date,
        before_date,
        name,
        states,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        let ids = get_identifiers(
          identifiers,
          after_date,
          before_date,
          name,
          states,
          backend,
          configuration,
        )
        .await?;
        Self::handle_abort(backend_client, &global_flags, ids).await
      }

      WorkflowSubcommands::Render {
        input_file,
        format,
        output_file,
        width,
        height,
        x_gap,
        y_gap,
      } => Self::handle_render(input_file, format, output_file, width, height, x_gap, y_gap),

      WorkflowSubcommands::Stop {
        backend,
        identifiers,
        after_date,
        before_date,
        name,
        states,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        let ids = get_identifiers(
          identifiers,
          after_date,
          before_date,
          name,
          states,
          backend,
          configuration,
        )
        .await?;
        Self::handle_stop(backend_client, &global_flags, ids).await
      }
    }
  }

  async fn handle_abort(
    mut backend_client: Client,
    global_flags: &GlobalFlags,
    identifiers: Vec<i64>,
  ) -> Result<()> {
    if global_flags.skip_acknowledgement
      || get_confirmation_from_vec(OPERATION_ABORT, "Workflow", &identifiers)
    {
      for identifier in identifiers.iter() {
        let response = backend_client
          .request(Action::AbortWorkflow {
            identifier: *identifier,
          })
          .await?;
        let message = if response.status().is_success() {
          "Workflow aborted "
        } else {
          "Failed abort workflow "
        };
        println!("{} (n°{})", message, identifier);
      }
    } else {
      println!("Cancelled operation");
    }
    Ok(())
  }

  async fn handle_list(
    mut backend_client: Client,
    output: &Output,
    after_date: &Option<DateTime<Utc>>,
    before_date: &Option<DateTime<Utc>>,
    name: &Option<String>,
    states: &[String],
    pagination: &PaginationOption,
  ) -> Result<()> {
    let response = backend_client
      .request(Action::ListWorkflow {
        after_date: *after_date,
        before_date: *before_date,
        name: name.clone(),
        states: states.to_owned(),
        pagination: Pagination::from(pagination.clone()),
      })
      .await?;
    if response.status().is_success() {
      print_response::<WorkflowListRenderer>(response, output).await?;
    } else {
      println!(
        "Something went wrong, got response status: {}",
        response.status()
      )
    }
    Ok(())
  }

  async fn handle_delete(
    mut backend_client: Client,
    global_flags: &GlobalFlags,
    identifiers: Vec<i64>,
  ) -> Result<()> {
    if global_flags.skip_acknowledgement
      || get_confirmation_from_vec(OPERATION_DELETE, "Workflow", &identifiers)
    {
      for identifier in identifiers.iter() {
        let response = backend_client
          .request(Action::DeleteWorkflow {
            identifier: *identifier,
          })
          .await?;
        let message = if response.status().is_success() {
          "Workflow deleted "
        } else {
          "Failed delete workflow "
        };
        println!("{} (n°{})", message, identifier);
      }
    } else {
      println!("Cancelled operation");
    }
    Ok(())
  }

  fn handle_render(
    input_file: &str,
    format: &OutputFormat,
    output_file: &str,
    width: &usize,
    height: &usize,
    x_gap: &usize,
    y_gap: &usize,
  ) -> Result<()> {
    let renderer = WorkflowRenderer::load_workflow(
      input_file,
      mcai_graph::GraphConfiguration::new(mcai_graph::NodeConfiguration::new(
        *width, *height, *x_gap, *y_gap,
      )),
    )?;

    match format {
      OutputFormat::SVG => {
        let svg_output = SvgOutput {}.render(renderer, WorkflowRendererOptions::default())?;
        std::fs::write(output_file, svg_output)?;
      }
    }

    Ok(())
  }

  async fn handle_show(
    mut backend_client: Client,
    output: &Output,
    identifiers: &[i64],
  ) -> Result<()> {
    if identifiers.is_empty() {
      println!("No workflow matches filter conditions");
    } else {
      for identifier in identifiers.iter() {
        let response = backend_client
          .request(Action::ShowWorkflow {
            identifier: *identifier,
            mode: ShowWorkflowMode::Simple,
          })
          .await?;
        print_response::<WorkflowShowRenderer>(response, output).await?;
      }
    }
    Ok(())
  }

  async fn handle_stop(
    mut backend_client: Client,
    global_flags: &GlobalFlags,
    identifiers: Vec<i64>,
  ) -> Result<()> {
    if global_flags.skip_acknowledgement
      || get_confirmation_from_vec(OPERATION_STOP, "Workflow", &identifiers)
    {
      for identifier in identifiers.iter() {
        let response = backend_client
          .request(Action::StopWorkflow {
            identifier: *identifier,
          })
          .await?;

        let message = if response.status().is_success() {
          "Workflow stopped "
        } else {
          "Failed stop workflow "
        };
        println!("{} (n°{})", message, identifier);
      }
    } else {
      println!("Cancelled operation");
    }
    Ok(())
  }
}

async fn get_identifiers(
  identifiers: &[i64],
  after_date: &Option<DateTime<Utc>>,
  before_date: &Option<DateTime<Utc>>,
  name: &Option<String>,
  states: &[String],
  backend: &str,
  configuration: Configuration,
) -> Result<Vec<i64>> {
  let mut ids = identifiers.to_vec();
  if ids.is_empty() {
    let filtered_ids = get_identifiers_from_filters(
      after_date,
      before_date,
      name,
      states,
      backend,
      configuration,
    )
    .await?;
    ids = filtered_ids;
  }
  Ok(ids)
}

pub async fn get_identifiers_from_filters(
  after_date: &Option<DateTime<Utc>>,
  before_date: &Option<DateTime<Utc>>,
  name: &Option<String>,
  states: &[String],
  backend: &str,
  configuration: Configuration,
) -> Result<Vec<i64>> {
  let mut backend_client = configuration.get_backend_client(backend)?;
  let response = backend_client
    .request(Action::ListWorkflow {
      after_date: *after_date,
      before_date: *before_date,
      name: name.clone(),
      states: states.to_owned(),
      //FIXME : what should be the value of pagination, to list all the elements and get the ids
      pagination: Pagination {
        page: 0,
        size: 10000,
      },
    })
    .await?;
  let workflow_multiple: WorkflowList = response.json().await?;
  let ids = workflow_multiple
    .data
    .iter()
    .map(|workflow| workflow.id as i64)
    .collect();
  Ok(ids)
}

#[cfg(test)]
mod tests {
  use std::vec;

  use super::*;
  use crate::configuration::Backend;
  use chrono::NaiveDate;
  use httpmock::prelude::*;
  use mcai_client::{AccessToken, WorkflowShow};
  use mcai_models::WorkflowInstance;

  #[tokio::test]
  async fn test_execute() {
    let expected_workflow = WorkflowShow {
      data: (WorkflowInstance {
        label: "Test workflow".to_string(),
        created_at: NaiveDate::from_ymd(2015, 9, 5).and_hms(23, 56, 4),
        id: 1,
        identifier: "Test_id".to_string(),
        is_live: false,
        jobs: vec![],
        reference: None,
        schema_version: "Test_scheme".to_string(),
        status: None,
        version_major: 9,
        version_minor: 9,
        version_micro: 9,
        start_parameters: vec![],
        steps: vec![],
        tags: vec![],
      }),
    };

    let server = MockServer::start();
    server.mock(|when, then| {
      when.method(GET).path("/api/step_flow/workflows/1");
      then
        .status(200)
        .header("content-type", "application/json")
        .body(serde_json::to_string(&expected_workflow).unwrap());
    });

    let access_token = AccessToken {
      access_token: "".to_string(),
      user: Default::default(),
    };
    server.mock(|when, then| {
      when.method(POST).path("/api/sessions");
      then
        .status(200)
        .header("content-type", "application/json")
        .body(serde_json::to_string(&access_token).unwrap());
    });

    let mut configuration = Configuration::default();
    configuration
      .add_backend(Backend::new(
        "backend_name",
        &server.base_url(),
        "user",
        "password",
      ))
      .unwrap();
    let output = Output::Raw;

    let credential_subcommand = WorkflowSubcommands::Show {
      backend: "backend_name".to_string(),
      identifiers: vec![1],
      output,
    };

    let global_flags = GlobalFlags {
      skip_acknowledgement: false,
    };

    let result = credential_subcommand.execute(configuration, global_flags);
    assert!(result.await.is_ok())
  }
}
