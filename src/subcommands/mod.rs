pub mod backend_configuration;
pub mod configuration;
pub mod credential;
pub mod user;
pub mod worker;
pub mod workflow;
use clap::Subcommand;
use std::{
  fmt::Display,
  io::{stdin, stdout, Write},
};
use {
  configuration::ConfigurationSubcommands, credential::CredentialSubcommands,
  user::UserSubcommands, worker::WorkerSubcommands, workflow::WorkflowSubcommands,
};

#[derive(Debug)]
pub struct GlobalFlags {
  pub(crate) skip_acknowledgement: bool,
}

#[derive(Subcommand)]
pub enum SubCommands {
  /// Credential Subcommands
  Credential {
    #[clap(subcommand)]
    command: CredentialSubcommands,
  },
  /// Configure credentials (Backend, Docker registries, etc.)
  #[clap(name = "config")]
  Configuration {
    #[clap(subcommand)]
    command: ConfigurationSubcommands,
  },
  /// Workflow Subcommands
  User {
    #[clap(subcommand)]
    command: UserSubcommands,
  },
  /// Worker Subcommands
  Worker {
    #[clap(subcommand)]
    command: WorkerSubcommands,
  },
  /// Workflow Subcommands
  Workflow {
    #[clap(subcommand)]
    command: WorkflowSubcommands,
  },
}

const CONFIRMATION_OK_VALUES: [&str; 2] = ["yes", "y"];

pub fn get_confirmation_from_vec<T: ToString>(
  operation: &str,
  selected_object: &str,
  vec_identifier: &[T],
) -> bool {
  get_confirmation(
    operation,
    selected_object,
    &vec_identifier
      .iter()
      .map(|id| id.to_string())
      .collect::<Vec<String>>()
      .join(", "),
  )
}

pub fn get_confirmation<T: Display>(
  operation: &str,
  selected_object: &str,
  identifier: &T,
) -> bool {
  println!(
    "Are you sure you want to {} {} {} (Yes/No) ?",
    operation, selected_object, identifier
  );
  CONFIRMATION_OK_VALUES.contains(&user_input().to_lowercase().as_str())
}

fn user_input() -> String {
  let mut s = String::new();
  let _ = stdout().flush();
  stdin()
    .read_line(&mut s)
    .expect("Did not enter a correct string");
  if let Some('\n') = s.chars().next_back() {
    s.pop();
  }
  if let Some('\r') = s.chars().next_back() {
    s.pop();
  }
  s
}

fn user_input_with_field_name(field: &str) -> String {
  println!("Enter {}: ", field);
  user_input()
}
