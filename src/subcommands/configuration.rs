use crate::{
  configuration::Configuration, error::Result,
  subcommands::backend_configuration::BackendSubcommands, GlobalFlags,
};
use clap::Subcommand;

#[derive(Debug, Subcommand)]
pub enum ConfigurationSubcommands {
  /// Backend Subcommand
  Backend {
    #[clap(subcommand)]
    command: BackendSubcommands,
  },
}

impl ConfigurationSubcommands {
  pub async fn execute(
    &self,
    configuration: Configuration,
    global_flags: GlobalFlags,
  ) -> Result<()> {
    match self {
      ConfigurationSubcommands::Backend { command } => command.execute(configuration, global_flags),
    }
  }
}
