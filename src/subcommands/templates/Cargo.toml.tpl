[package]
name = "mcai_worker"
version = "0.0.1"
authors = []
description = "Custom Worker"
license = "MIT"
readme = "README.md"
edition = "2018"

[dependencies]
log = "0.4"
mcai_worker_sdk = "2.0.0-rc0"
schemars = "0.8.0"
serde = { version = "1.0", features = ["derive"] }
serde_json = "1.0.82"

[build-dependencies]
mcai_build = "0.1.2"
