use mcai_worker_sdk::prelude::*;
use serde::Deserialize;
use schemars::JsonSchema;

struct WorkerNameWorkerContext {}

#[derive(Debug, Deserialize, JsonSchema)]
struct WorkerNameWorkerParameters {
  // Add worker parameters here
}

// Call the macro that create the structure RustMcaiWorkerDescription.
// It retrieves information from the Cargo.toml of the worker.
//
// default usage of the macro for open source workers
default_rust_mcai_worker_description!();
// Change to that line for Private worker
// default_rust_mcai_worker_description!(Private);
// Change to that line for Commercial worker
// default_rust_mcai_worker_description!(Commercial);

impl McaiWorker<WorkerNameWorkerParameters, RustMcaiWorkerDescription> for WorkerNameWorkerContext {
  fn process(
    &self,
    _channel: Option<McaiChannel>,
    _parameters: WorkerNameWorkerParameters,
    job_result: JobResult,
  ) -> Result<JobResult> {
    // @TODO implement the worker process here
    Ok(job_result)
  }
}

fn main() {
  start_worker(WorkerNameWorkerContext{});
}
