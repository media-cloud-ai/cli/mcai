use crate::{
  configuration::Configuration,
  constant::*,
  error::Result,
  get_confirmation,
  pagination::PaginationOption,
  print_response,
  renderer::{BenchmarksListRenderer, Render, WorkerListRenderer, WorkerShowRenderer},
  subcommands::GlobalFlags,
  Output,
};
use cargo_toml::Manifest;
use chrono::Utc;
use clap::Subcommand;
use mcai_benchmark::{
  benchmark::result::Results,
  configuration::{Configuration as BenchmarkConfig, Version1},
  report::writer::GraphWriter,
};
use mcai_client::{Action, Client, Pagination, WorkerList};
use mcai_docker::image;
use std::fs::File;
use std::{
  io::{stdout, Cursor, Write},
  str::from_utf8,
  sync::{Arc, Mutex},
};

#[derive(Debug, Subcommand)]
pub enum WorkerSubcommands {
  /// List workers
  List {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets identifiers
    #[clap(short, long, name = "identifiers")]
    instance_id: Option<String>,
    /// Sets job id filter
    #[clap(short, long)]
    job_id: Option<String>,
    /// Configure the rendering output
    #[clap(short, default_value = "formatted", parse(from_str))]
    output: Output,
    #[clap(flatten)]
    pagination: PaginationOption,
  },
  New {
    /// Name of the worker that will be created
    #[clap(short, long, required = true)]
    name: String,
  },
  /// Show one or more specified workers
  Show {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets identifiers
    #[clap(multiple_values = true, index = 1, required = true)]
    identifiers: Vec<String>,
    /// Configure the rendering output
    #[clap(short, default_value = "formatted", parse(from_str))]
    output: Output,
  },
  /// Stop one or more specified workers
  Stop {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets identifiers
    #[clap(name = "identifiers", required_unless_present = "job_id")]
    instance_id: Vec<String>,
    /// Sets job id filter
    #[clap(name = "job_id", required_unless_present = "identifiers", long, short)]
    job_id: Vec<String>,
  },
  /// Stop consuming jobs for one or more specified worker
  #[clap(name = "lock")]
  ConsumingJobStop {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets identifiers
    #[clap(name = "identifiers", required_unless_present = "job_id")]
    instance_id: Vec<String>,
    /// Sets job id filter
    #[clap(name = "job_id", required_unless_present = "identifiers", long, short)]
    job_id: Vec<String>,
  },
  /// Resume consuming jobs for one or more specified workers
  #[clap(name = "unlock")]
  ConsumingJobResume {
    /// Sets the backend
    #[clap(long, default_value = "default")]
    backend: String,
    /// Sets identifiers
    #[clap(name = "identifiers", required_unless_present = "job_id")]
    instance_id: Vec<String>,
    /// Sets job id filter
    #[clap(name = "job_id", required_unless_present = "identifiers", long, short)]
    job_id: Vec<String>,
  },
  Describe {
    /// Name of the worker to get describe from
    #[clap(short, long, required = true)]
    image: String,
    /// Path where to store describe output
    #[clap(long)]
    output_folder: Option<String>,
  },
  Benchmark {
    /// Path to the configuration file
    #[clap(short, long, required = true)]
    config: String,
  },
  ListBenchmarks {
    /// Name of the worker to retrieve benchmarks from
    #[clap(short, long, required = true)]
    image: String,
  },
}

impl WorkerSubcommands {
  pub async fn execute(
    &self,
    configuration: Configuration,
    global_flags: GlobalFlags,
  ) -> Result<()> {
    match self {
      WorkerSubcommands::List {
        instance_id,
        job_id,
        output,
        backend,
        pagination,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        Self::handle_list(backend_client, instance_id, job_id, output, pagination).await
      }
      WorkerSubcommands::New { name } => {
        let cargo_toml_template = include_str!("templates/Cargo.toml.tpl");
        let mut cargo_toml = Manifest::from_str(cargo_toml_template).unwrap();

        let rust_name = format!("rs_{}_worker", heck::AsSnakeCase(name));

        if let Some(package) = &mut cargo_toml.package {
          package.name = rust_name.clone();
          package.description = Some(format!("{} Worker", name));
        }

        let worker_directory = std::path::Path::new(&rust_name);

        std::fs::create_dir(worker_directory)?;
        std::fs::write(
          worker_directory.join("Cargo.toml"),
          toml::to_string(&cargo_toml).unwrap(),
        )?;
        std::fs::write(
          worker_directory.join("Dockerfile"),
          include_str!("templates/Dockerfile")
            .replace("WORKER_NAME", &heck::AsSnakeCase(name).to_string()),
        )?;
        std::fs::write(
          worker_directory.join(".gitignore"),
          include_str!("templates/.gitignore"),
        )?;
        std::fs::write(
          worker_directory.join("build.rs"),
          include_str!("templates/build.rs"),
        )?;
        std::fs::write(worker_directory.join("rustfmt.toml"), "tab_spaces = 2\n")?;
        std::fs::write(
          worker_directory.join("README.md"),
          format!(
            "# {} Worker\n\nMedia Cloud AI worker",
            heck::AsTitleCase(name)
          ),
        )?;

        let src_worker_directory = worker_directory.join("src");
        std::fs::create_dir(src_worker_directory.clone())?;

        std::fs::write(
          src_worker_directory.join("main.rs"),
          include_str!("templates/main_worker.rs")
            .replace("WorkerName", &heck::AsUpperCamelCase(name).to_string()),
        )?;

        Ok(())
      }
      WorkerSubcommands::Show {
        identifiers: ids,
        output,
        backend,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        Self::handle_show(backend_client, ids, output).await
      }
      WorkerSubcommands::Stop {
        backend,
        instance_id,
        job_id,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        let ids = get_identifiers_from_filters(instance_id, job_id, backend, configuration).await?;
        Self::handle_stop(backend_client, &global_flags, ids).await
      }
      WorkerSubcommands::ConsumingJobStop {
        backend,
        instance_id,
        job_id,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        let ids = get_identifiers_from_filters(instance_id, job_id, backend, configuration).await?;
        Self::handle_lock(backend_client, &global_flags, ids).await
      }
      WorkerSubcommands::ConsumingJobResume {
        backend,
        instance_id,
        job_id,
      } => {
        let backend_client = configuration.get_backend_client(backend)?;
        let ids = get_identifiers_from_filters(instance_id, job_id, backend, configuration).await?;
        Self::handle_unlock(backend_client, &global_flags, ids).await
      }
      WorkerSubcommands::Describe {
        image,
        output_folder,
      } => Self::handle_describe(image, output_folder.clone()).await,
      WorkerSubcommands::ListBenchmarks { image } => Self::handle_list_benchmarks(image).await,
      WorkerSubcommands::Benchmark { config } => Self::handle_benchmark(config).await,
    }
  }

  async fn handle_list(
    mut backend_client: Client,
    instance_id: &Option<String>,
    job_id: &Option<String>,
    output: &Output,
    pagination: &PaginationOption,
  ) -> Result<()> {
    let response = backend_client
      .request(Action::ListWorker {
        identifier: instance_id.clone(),
        job_id: job_id.clone(),
        pagination: Pagination::from(pagination.clone()),
      })
      .await?;
    if response.status().is_success() {
      print_response::<WorkerListRenderer>(response, output).await?;
    } else {
      println!(
        "Something went wrong, got response status: {}",
        response.status()
      )
    }
    Ok(())
  }

  async fn handle_show(
    mut backend_client: Client,
    identifiers: &[String],
    output: &Output,
  ) -> Result<()> {
    if identifiers.is_empty() {
      println!("No worker matches filter conditions");
    } else {
      for id in identifiers {
        let response = backend_client
          .request(Action::ShowWorker {
            identifier: id.to_string(),
          })
          .await;
        let _ = print_response::<WorkerShowRenderer>(response.unwrap(), output).await;
      }
    }
    Ok(())
  }

  async fn handle_stop(
    mut backend_client: Client,
    global_flags: &GlobalFlags,
    ids: Vec<String>,
  ) -> Result<()> {
    if ids.is_empty() {
      println!("No worker matches filter conditions");
    } else if global_flags.skip_acknowledgement
      || get_confirmation(OPERATION_STOP, "Worker", &ids.join(", "))
    {
      for identifier in ids.iter() {
        let response = backend_client
          .request(Action::StopWorker {
            identifier: identifier.clone(),
          })
          .await?;
        let message = if response.status().is_success() {
          "Worker job consumption stopped"
        } else {
          "Failed to stop workers job consumption"
        };
        println!("{} (id : {})", message, identifier);
      }
    } else {
      println!("Cancelled operation");
    }
    Ok(())
  }

  async fn handle_lock(
    mut backend_client: Client,
    global_flags: &GlobalFlags,
    ids: Vec<String>,
  ) -> Result<()> {
    if ids.is_empty() {
      println!("No worker matches filter conditions");
    } else if global_flags.skip_acknowledgement
      || get_confirmation(OPERATION_JOB_CONSUMPTION_STOP, "Worker", &ids.join(", "))
    {
      for identifier in ids.iter() {
        let response = backend_client
          .request(Action::WorkerJobConsumptionStop {
            identifier: identifier.clone(),
          })
          .await?;
        let message = if response.status().is_success() {
          "Worker job consumption locked (ID {})"
        } else {
          "Failed to lock workers job consumption (ID {})"
        };
        println!("{} (n°{})", message, identifier);
      }
    } else {
      println!("Cancelled operation");
    }
    Ok(())
  }

  async fn handle_unlock(
    mut backend_client: Client,
    global_flags: &GlobalFlags,
    ids: Vec<String>,
  ) -> Result<()> {
    if ids.is_empty() {
      println!("No worker matches filter conditions");
    } else if global_flags.skip_acknowledgement
      || get_confirmation(OPERATION_JOB_CONSUMPTION_RESUME, "Worker", &ids.join(", "))
    {
      for identifier in ids.iter() {
        let response = backend_client
          .request(Action::WorkerJobConsumptionResume {
            identifier: identifier.clone(),
          })
          .await?;
        let message = if response.status().is_success() {
          "Worker job consumption unlocked"
        } else {
          "Failed to unlock workers job consumption"
        };
        println!("{} (n°{})", message, identifier);
      }
    } else {
      println!("Cancelled operation");
    }
    Ok(())
  }

  fn open_writer(image: &str, output_folder: &Option<String>) -> Result<Box<dyn Write + Send>> {
    if let Some(output_folder) = output_folder {
      let filepath = format!(
        "{}/{}_{}.json",
        output_folder,
        image,
        Utc::now().format("%Y%m%d_%H%M%S")
      );
      Ok(Box::new(
        File::options().create(true).append(true).open(filepath)?,
      ))
    } else {
      Ok(Box::new(stdout()))
    }
  }

  async fn handle_describe(image: &str, output_folder: Option<String>) -> Result<()> {
    let docker = mcai_docker::Docker::connect_with_socket_defaults().unwrap();

    let builder = image::Image::new(image).build(&docker).await?;
    let container = builder.with_env("DESCRIBE", &1).build(&docker).await?;

    let writer = Self::open_writer(image, &output_folder)?;

    container.run(&docker, Some(Box::leak(writer))).await?;
    container.remove(&docker, true).await?;
    Ok(())
  }

  async fn handle_list_benchmarks(image: &str) -> Result<()> {
    let docker = mcai_docker::Docker::connect_with_socket_defaults().unwrap();

    let builder = image::Image::new(image).build(&docker).await?;
    let container = builder
      .with_command("ls -1 /examples")
      .build(&docker)
      .await?;

    let mut buff = Cursor::new(vec![]);

    container.run(&docker, Some(&mut buff)).await?;

    let mut benchmarks = Vec::new();

    if let Ok(m) = from_utf8(&buff.into_inner()) {
      benchmarks = m
        .to_string()
        .split_terminator('\n')
        .filter(|b| !b.contains("ls: /examples: No such file or directory"))
        .map(|b| b.to_string().replace(".json", ""))
        .collect();
    }

    BenchmarksListRenderer { benchmarks }.render();

    container.remove(&docker, true).await?;
    Ok(())
  }

  async fn handle_benchmark(configuration_file: &str) -> Result<()> {
    let config = Version1::read_from_file(configuration_file).unwrap();
    let mut benchmark = Results::from(BenchmarkConfig::Version1(config));

    benchmark
      .run_benchmark(Some(Arc::new(Mutex::new(GraphWriter::new()))))
      .await?;
    Ok(())
  }
}

async fn get_identifiers_from_filters(
  instance_id: &[String],
  job_id: &[String],
  backend: &str,
  configuration: Configuration,
) -> Result<Vec<String>> {
  if instance_id.is_empty() && job_id.is_empty() {
    Ok(vec![])
  } else if job_id.is_empty() {
    //todo : filters the "doublons"
    Ok(instance_id.to_vec())
  } else {
    let mut backend_client = configuration.get_backend_client(backend)?;
    let mut instance_ids = instance_id.to_vec();
    for id in job_id {
      let response = backend_client
        .request(Action::ListWorker {
          identifier: None,
          job_id: Some(id.clone()),
          //FIXME what should be the value of pagination, to list all the elements and get the ids
          pagination: Pagination {
            page: 0,
            size: 10000,
          },
        })
        .await?;
      let worker_multiple: WorkerList = response.json().await?;
      let mut worker_multiple_list_id = worker_multiple
        .data
        .iter()
        .map(|worker| worker.instance_id.clone())
        .collect::<Vec<String>>();

      instance_ids.append(&mut worker_multiple_list_id)
    }
    instance_ids.sort();
    instance_ids.dedup();

    Ok(instance_ids)
  }
}
