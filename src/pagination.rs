use clap::Args;
use mcai_client::Pagination;
use serde::{Deserialize, Serialize};

#[derive(Args, Clone, Debug, Deserialize, Serialize)]
pub struct PaginationOption {
  /// Sets page
  #[clap(short = 'P', default_value = "0")]
  pub(crate) page: u64,
  /// Sets size per page
  #[clap(short = 'S', default_value = "10")]
  pub(crate) size: u64,
}

impl From<PaginationOption> for Pagination {
  fn from(pagination_option: PaginationOption) -> Self {
    Pagination {
      page: pagination_option.page,
      size: pagination_option.size,
    }
  }
}
