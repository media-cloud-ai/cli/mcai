#[allow(dead_code)]
pub mod built_info {
  include!(concat!(env!("OUT_DIR"), "/built.rs"));
}

pub static OPERATION_ABORT: &str = "abort";
pub static OPERATION_DELETE: &str = "delete";
pub static OPERATION_STOP: &str = "stop";
pub static OPERATION_JOB_CONSUMPTION_STOP: &str = "lock";
pub static OPERATION_JOB_CONSUMPTION_RESUME: &str = "unlock";

pub static HIDDEN_VALUE: &str = "******";
